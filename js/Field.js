/**
 * Created by Sam.
 */
"use strict";

var Field = function () {
    this.column = null;
    this.row = null;
    this.size = 10;
    this.delta = 2;
    this.arrCase = [];
};

Field.prototype.setSize = function (column, row) {
    this.column = column / this.size;
    this.row = row / this.size;
};

Field.prototype.getArr = function () {
    return this.arrCase;
};

Field.prototype.getSize = function () {
    return this.size;
};

Field.prototype.setArr = function (arr) {
    for (var i = 0; i < this.column; i++) {
        for (var j = 0; j < this.row; j++) {
            this.arrCase[i][j].setBusy(arr[i][j].getBusy());
        }
    }

};

Field.prototype.getColumn = function () {
    return this.column;
};

Field.prototype.getRow = function () {
    return this.row;
};

Field.prototype.generateArr = function () {

    for (var i = 0; i < this.column; i++) {
        this.arrCase[i] = [];
        for (var j = 0; j < this.row; j++) {
            this.arrCase[i][j] = new Case(i, j);
        }
    }

};

Field.prototype.render = function (ctx, color) {
    for (var i = 0; i < this.column; i++) {
        for (var j = 0; j < this.row; j++) {
            this.arrCase[i][j].render(ctx, color, this.size, this.delta);
        }
    }
};

Field.prototype.setCase = function (x, y) {
    this.arrCase[x][y].setBusy(!this.arrCase[x][y].getBusy());
};

Field.prototype.reset = function () {
    for (var i = 0; i < this.column; i++) {
        for (var j = 0; j < this.row; j++) {
            this.arrCase[i][j].setBusy(false);
        }
    }
};