/**
 * Created by Sam.
 */
"use strict";
var Case = function (x, y) {
    this.x = x;
    this.y = y;
    this.busy = false;
};

Case.prototype.render = function (ctx, color, size, delta) {
    ctx.fillStyle = this.busy ? color : "white";
    ctx.fillRect(this.x * size + delta, this.y * size + delta, size - delta, size - delta);

};

Case.prototype.getBusy = function () {
    return this.busy;
};

Case.prototype.setBusy = function (busy) {
    this.busy = busy;
};
