/**
 * Created by Sam on 15.06.2017.
 */
var canvas, ctx;
var width, height, size, delta, column, row;
var arr = [], copyArr = [];
var game = {time: null};
var isDown = false;
var curX, curY, firstEnter = false;
window.onload = function () {
    init();
    render();

    $('#start_game').on('click', function () {
        if (game.time == null) {
            game.time = setInterval(process, 50);
        }
    });

    $('#stop_game').on('click', function () {
        if (game.time != null) {
            clearInterval(game.time);
            game.time = null;
        }
    });

    $('#clear_game').on('click', function () {
        if (game.time != null) {
            clearInterval(game.time);
            game.time = null;
        }

    });

    $('#field').on('mousedown', function (e) {

        if (isDown === false) {
            isDown = true;
            firstEnter = true;
        }
    });

    $('#field').on('mousemove', function (e) {

        if (isDown === true) {

            var pos = getMousePos(canvas, e);
            var x = Math.floor(pos.x / size);
            var y = Math.floor(pos.y / size);

            if (firstEnter === true) {
                arr[x][y] = arr[x][y] === 0 ? 1 : 0;
                curX = x;
                curY = y;
                firstEnter = false;
                render();
            } else {
                firstEnter = !(curY === y && curX === x);
            }
        }
    });

    $('#field').on('mouseup', function (e) {
        if (isDown === true) {
            isDown = false;
        }
    });

};

function init() {
    canvas = document.getElementById('field');
    ctx = canvas.getContext('2d');
    width = canvas.width = 800;
    height = canvas.height = 400;
    size = 10;
    delta = 2;
    column = width / 10;
    row = height / 10;
    for (var i = 0; i < column; i++) {
        arr[i] = [];
        copyArr[i] = [];
        for (var j = 0; j < row; j++) {
            arr[i][j] = 0;
            copyArr[i][j] = 0
            //arr[i][j] = getRandomInt(0, 2);
        }
    }

    arr[2][0] = 1;
    arr[2][1] = 1;
    arr[2][2] = 1;
    arr[0][1] = 1;
    arr[1][2] = 1;
}

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}

function process() {
    liveOrDie();
    copyCopyToArr(arr, copyArr, column, row);
    render();
}

function liveOrDie() {
    for (var i = 0; i < column; i++) {
        for (var j = 0; j < row; j++) {
            var count = countNeighbor(i, j);
            if (arr[i][j] === 0) {
                if (count === 3) {
                    copyArr[i][j] = 1;
                }
            } else {
                if (count < 2 || count > 3) {
                    copyArr[i][j] = 0;
                } else {
                    copyArr[i][j] = 1;
                }
            }
        }
    }
}

function countNeighbor(i, j) {
    var count = 0;

    if (i === 0) { // column 0
        if (j === 0) { // row 0
            count += arr[i + 1][j] + arr[i][j + 1] + arr[i + 1][j + 1];
        } else {
            if (j === row - 1) { // row m
                count = arr[i + 1][j] + arr[i][j - 1] + arr[i + 1][j - 1];
            } else { // left
                count = arr[i + 1][j] + arr[i][j - 1] + arr[i + 1][j - 1] + arr[i][j + 1] + arr[i + 1][j + 1];
            }
        }
    } else {

        if (i === column - 1) { // column - n

            if (j === 0) { // row 0
                count = arr[i - 1][j] + arr[i][j + 1] + arr[i - 1][j + 1];
            } else {
                if (j === row - 1) { // row - m
                    count = arr[i - 1][j] + arr[i][j - 1] + arr[i - 1][j - 1];
                } else { // right
                    count = arr[i][j - 1] + arr[i - 1][j - 1] + arr[i - 1][j] + arr[i][j + 1] + arr[i - 1][j + 1];
                }
            }
        } else { // center

            if (j === 0) { // row - 0
                count = arr[i - 1][j] + arr[i - 1][j + 1] + arr[i][j + 1] + arr[i + 1][j + 1] + arr[i + 1][j];
            } else {
                if (j === row) { // row - m
                    count = arr[i - 1][j] + arr[i - 1][j - 1] + arr[i][j - 1] + arr[i + 1][j - 1] + arr[i + 1][j];
                } else { // center
                    count = arr[i + 1][j] + arr[i + 1][j + 1] + arr[i][j + 1] + arr[i - 1][j + 1] + arr[i - 1][j] + arr[i - 1][j - 1] + arr[i][j - 1] + arr[i + 1][j - 1];
                }
            }
        }
    }
    return count;
}

function render() {
    canvas.width = canvas.width;
    for (var i = 0; i < column; i++) {
        for (var j = 0; j < row; j++) {
            if (arr[i][j] === 1) {
                drawRectWithColor(i, j, 'green')
            }
            else {
                drawRectWithColor(i, j, 'white')
            }
        }
    }
}

//-------------------------------------------//

function copyCopyToArr(arr, copy, n, m) {
    for (var i = 0; i < n; i++) {
        for (var j = 0; j < m; j++) {
            arr[i][j] = copy[i][j];
        }
    }
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function drawRectWithColor(x, y, color) {
    ctx.fillStyle = color;
    ctx.fillRect(x * size + delta, y * size + delta, size - delta, size - delta);
}
