/**
 * Created by Sam.
 */
var canvas = document.getElementById('field');
var game = new Game();
var curX, curY, isDown = false, firstEnter = false;

canvas.width = 800;
canvas.height = 400;

game.init(canvas, canvas.width, canvas.height);

$('#start_game').on('click', function () {
    game.start();
});

$('#stop_game').on('click', function () {
    game.stop();
});

$('#reset_game').on('click', function () {
    game.reset();
});

$('#field').on('mousedown', function (e) {

    if (isDown === false) {
        isDown = true;
        firstEnter = true;
    }
});

$('#field').on('mousemove', function (e) {

    if (isDown === true) {
        var size = game.getSize();
        var pos = getMousePos(canvas, e);
        var x = Math.floor(pos.x / size);
        var y = Math.floor(pos.y / size);

        if (firstEnter === true) {
            game.setCase(x, y);
            curX = x;
            curY = y;
            firstEnter = false;
            game.render();
        } else {
            firstEnter = !(curY === y && curX === x);
        }
    }
});

$('#field').on('mouseup', function (e) {
    if (isDown === true) {
        isDown = false;
    }
});


function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}