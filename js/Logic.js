/**
 * Created by Sam.
 */
var Logic = function () {
    this.copyArr = [];
};

Logic.prototype.countNeighbor = function (arr, column, row, i, j) {

    var count = 0;

    if (i === 0) { // column 0
        if (j === 0) { // row 0
            count += arr[i + 1][j].getBusy() + arr[i][j + 1].getBusy() + arr[i + 1][j + 1].getBusy();
        } else {
            if (j === row - 1) { // row m
                count = arr[i + 1][j].getBusy() + arr[i][j - 1].getBusy() + arr[i + 1][j - 1].getBusy();
            } else { // left
                count = arr[i + 1][j].getBusy() + arr[i][j - 1].getBusy() + arr[i + 1][j - 1].getBusy() + arr[i][j + 1].getBusy() + arr[i + 1][j + 1].getBusy();
            }
        }
    } else {
        if (i === column - 1) { // column - n
            if (j === 0) { // row 0
                count = arr[i - 1][j].getBusy() + arr[i][j + 1].getBusy() + arr[i - 1][j + 1].getBusy();
            } else {
                if (j === row - 1) { // row - m
                    count = arr[i - 1][j].getBusy() + arr[i][j - 1].getBusy() + arr[i - 1][j - 1].getBusy();
                } else { // right
                    count = arr[i][j - 1].getBusy() + arr[i - 1][j - 1].getBusy() + arr[i - 1][j].getBusy() + arr[i][j + 1].getBusy() + arr[i - 1][j + 1].getBusy();
                }
            }
        } else { // center

            if (j === 0) { // row - 0
                count = arr[i - 1][j].getBusy() + arr[i - 1][j + 1].getBusy() + arr[i][j + 1].getBusy() + arr[i + 1][j + 1].getBusy() + arr[i + 1][j].getBusy();
            } else {

                if (j === row - 1) { // row - m
                    count = arr[i - 1][j].getBusy() + arr[i - 1][j - 1].getBusy() + arr[i][j - 1].getBusy() + arr[i + 1][j - 1].getBusy() + arr[i + 1][j].getBusy();
                } else { // center
                    count = arr[i + 1][j].getBusy() + arr[i + 1][j + 1].getBusy() + arr[i][j + 1].getBusy() + arr[i - 1][j + 1].getBusy() + arr[i - 1][j].getBusy() + arr[i - 1][j - 1].getBusy() + arr[i][j - 1].getBusy() + arr[i + 1][j - 1].getBusy();
                }
            }
        }
    }
    return count;
};

Logic.prototype.liveOrDie = function (arr, column, row) {

    this.copyArr = [];
    console.log(column, row);
    for (var i = 0; i < column; i++) {
        this.copyArr[i] = [];
        for (var j = 0; j < row; j++) {
            var count = this.countNeighbor(arr, column, row, i, j);
            this.copyArr[i][j] = new Case(i, j);
            if (arr[i][j].getBusy() == false) {
                if (count === 3) {
                    this.copyArr[i][j].setBusy(true);
                }
            } else {
                if (count < 2 || count > 3) {
                    this.copyArr[i][j].setBusy(false);
                } else {
                    this.copyArr[i][j].setBusy(true);
                }
            }
        }
    }
    return this.copyArr;
};

