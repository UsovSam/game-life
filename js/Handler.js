/**
 * Created by Sam.
 */
"use strict";

$('#start_game').on('click', function () {

    if (game.time == null) {
        game.time = setInterval(process, 50);
    }

});

$('#stop_game').on('click', function () {

    if (game.time != null) {
        clearInterval(game.time);
        game.time = null;
    }
});

$('#clear_game').on('click', function () {

    if (game.time != null) {
        clearInterval(game.time);
        game.time = null;
    }

});