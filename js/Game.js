/**
 * Created by Sam.
 */
"use strict";
var Game = function () {
    this.process = new Process();
    this.time = null;
    this.canvas = null;
    this.ctx = null;
};

Game.prototype.init = function (canvas, width, height) {
    this.process.initField(width, height);
    this.canvas = canvas;
    this.ctx = this.canvas.getContext('2d');
    this.process.render(this.ctx, "green");
};

Game.prototype.render = function () {
    this.process.render(this.ctx, "green");
};

Game.prototype.start = function () {
    if (this.time == null) {
        var self = this;
        this.time = setInterval(function () {
            self.process.processGame(self.canvas, self.ctx, "green");
        }, 1000);
    }
};

Game.prototype.stop = function () {
    if (this.time != null) {
        clearInterval(this.time);
        this.time = null;
    }
};

Game.prototype.reset = function () {
    if (this.time != null) {
        clearInterval(this.time);
        this.time = null;
    }
    this.process.reset();
    this.canvas.width = this.canvas.width;
    this.render();

};

Game.prototype.setCase = function (x, y) {
    this.process.setCase(x, y)
};

Game.prototype.getSize = function () {
    return this.process.getSize();
};


