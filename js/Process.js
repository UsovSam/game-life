/**
 * Created by Sam.
 */
"use strict";
var Process = function () {
    this.logic = new Logic();
    this.field = new Field();
};

Process.prototype.initField = function (width, height) {
    this.field.setSize(width, height);
    this.field.generateArr();
};

Process.prototype.render = function (ctx, color) {
    this.field.render(ctx, color);
};

Process.prototype.processGame = function (canvas, ctx, color) {
    var field = this.field;
    var arr = this.logic.liveOrDie(field.getArr(), field.getColumn(), field.getRow());
    this.field.setArr(arr);
    canvas.width = canvas.width;
    this.render(ctx, color);
};

Process.prototype.setCase = function (x, y) {
    this.field.setCase(x, y)
};

Process.prototype.getSize = function () {
    return this.field.getSize();
};

Process.prototype.reset = function () {
    this.field.reset();
};
